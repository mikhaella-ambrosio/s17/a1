console.log(`ACTIVITY S17`)

let studentNames = [];
// console.log(studentNames);

function addStudent(name) {
	console.log(`${name} was added to the student's list.`)

	return studentNames.push(name)
}

function countStudents(){
	console.log(`There are a total of ${studentNames.length} students enrolled.`)
}

function printStudents(){

	let mapStudents = studentNames.map (
		function(student){
			console.log(student)
		}
	).join (" ")
}

function checkStudent (studentIncluded){
	if (studentNames.includes(studentIncluded)){
		return `${studentIncluded} is an enrolee.`
	}
	else {
		return `No student found with the name ${studentIncluded}.`
	}
}


